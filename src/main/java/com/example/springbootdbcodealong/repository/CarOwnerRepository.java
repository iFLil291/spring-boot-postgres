package com.example.springbootdbcodealong.repository;

import com.example.springbootdbcodealong.domain.CarOwner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarOwnerRepository extends JpaRepository<CarOwner, String> {

}
