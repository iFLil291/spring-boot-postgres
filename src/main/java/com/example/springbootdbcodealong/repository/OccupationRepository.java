package com.example.springbootdbcodealong.repository;

import com.example.springbootdbcodealong.domain.Occupation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
public interface OccupationRepository extends JpaRepository<Occupation, String> {
    Stream<Occupation> findByLabel(String label);
}
