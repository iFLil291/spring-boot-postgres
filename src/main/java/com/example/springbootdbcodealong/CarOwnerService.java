package com.example.springbootdbcodealong;

import com.example.springbootdbcodealong.domain.Address;
import com.example.springbootdbcodealong.domain.Car;
import com.example.springbootdbcodealong.domain.CarOwner;
import com.example.springbootdbcodealong.domain.Occupation;
import com.example.springbootdbcodealong.exceptions.CarOwnerHasCarException;
import com.example.springbootdbcodealong.exceptions.CarOwnerNotFoundException;
import com.example.springbootdbcodealong.repository.CarOwnerRepository;
import com.example.springbootdbcodealong.repository.OccupationRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class CarOwnerService {
    CarOwnerRepository carOwnerRepository;
    OccupationRepository occupationRepository;

    public Stream<CarOwner> all() {
        return carOwnerRepository.findAll().stream();
    }

    public CarOwner get(String id) throws CarOwnerNotFoundException {
        return carOwnerRepository.findById(id)
                .orElseThrow(() -> new CarOwnerNotFoundException("Unable to find car owner with id " + id));
    }

    public CarOwner create(String name) {
        CarOwner carOwner = new CarOwner(name);
        return carOwnerRepository.save(carOwner);
    }

    public CarOwner update(String id, String name)  throws CarOwnerNotFoundException {
        CarOwner carOwner = get(id);
        carOwner.setName(name);
        return carOwnerRepository.save(carOwner);
    }

    public void delete(String id) throws CarOwnerNotFoundException {
        carOwnerRepository.delete(get(id));
    }

    public CarOwner addCar(String id, String brand) throws CarOwnerNotFoundException, CarOwnerHasCarException {
        CarOwner carOwner = get(id);
        if (carOwner.car().isPresent()) {
            throw new CarOwnerHasCarException();
        }
        Car car = new Car(brand, carOwner);
        carOwner.setCar(car);
        return carOwnerRepository.save(carOwner);
    }

    public CarOwner addAddress(String id, String street) throws CarOwnerNotFoundException {
        CarOwner carOwner = get(id);
        Address address = new Address(street, carOwner);
        carOwner.addAddress(address);
        return carOwnerRepository.save(carOwner);
    }

    public CarOwner addOccupation(String id, String label) throws CarOwnerNotFoundException {
        CarOwner carOwner = get(id);
        Occupation occupation = occupationRepository.findByLabel(label)
                .findAny()
                .orElse(new Occupation(label));
        if (carOwner.hasOccupation(occupation)) {
            return carOwner;
        }
        occupation.addCarOwner(carOwner);
        carOwner.addOccupation(occupation);
        return carOwnerRepository.save(carOwner);
    }
}
