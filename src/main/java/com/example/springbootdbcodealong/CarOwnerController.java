package com.example.springbootdbcodealong;

import com.example.springbootdbcodealong.domain.Address;
import com.example.springbootdbcodealong.domain.Car;
import com.example.springbootdbcodealong.domain.CarOwner;
import com.example.springbootdbcodealong.domain.Occupation;
import com.example.springbootdbcodealong.exceptions.CarOwnerHasCarException;
import com.example.springbootdbcodealong.exceptions.CarOwnerNotFoundException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/1.0/car_owners")
@AllArgsConstructor
public class CarOwnerController {

    CarOwnerService carOwnerService;

    @GetMapping
    public List<CarOwnerDTO> all() {
        return carOwnerService.all()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<CarOwnerDTO> get(@PathVariable("id") String id) {
        try {
            return ResponseEntity.ok(
                    toDTO(
                            carOwnerService.get(id)));
        } catch (CarOwnerNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public CarOwnerDTO create(@RequestBody CreateCarOwnerDTO createCarOwnerDTO) {
        return toDTO(carOwnerService.create(
                createCarOwnerDTO.getName()
        ));
    }

    @PutMapping("/{id}")
    public ResponseEntity<CarOwnerDTO> update(@PathVariable("id") String id,
                                              @RequestBody UpdateCarOwnerDTO updateCarOwnerDTO) {
        try {
            return ResponseEntity.ok(
                    toDTO(carOwnerService.update(
                            id,
                            updateCarOwnerDTO.getName())));
        } catch (CarOwnerNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/{id}/addCar")
    public ResponseEntity<CarOwnerDTO> addCar(@PathVariable("id") String id,
                                              @RequestBody AddCarDTO addCarDTO) {
        try {
            return ResponseEntity.ok(
                    toDTO(carOwnerService.addCar(
                            id,
                            addCarDTO.getBrand())));
        } catch (CarOwnerNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (CarOwnerHasCarException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/{id}/addAddress")
    public ResponseEntity<CarOwnerDTO> addAddress(@PathVariable("id") String id,
                                                  @RequestBody AddAddressDTO addAddressDTO) {
        try {
            return ResponseEntity.ok(
                    toDTO(carOwnerService.addAddress(
                            id,
                            addAddressDTO.getStreet())));
        } catch (CarOwnerNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/{id}/addOccupation")
    public ResponseEntity<CarOwnerDTO> addOccupation(@PathVariable("id") String id,
                                                     @RequestBody AddOccupationDTO addOccupationDTO) {
        try {
            return ResponseEntity.ok(
                    toDTO(carOwnerService.addOccupation(
                            id,
                            addOccupationDTO.getLabel())));
        } catch (CarOwnerNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") String id) {
        try {
            carOwnerService.delete(id);
            return ResponseEntity.ok().build();
        } catch (CarOwnerNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    private CarOwnerDTO toDTO(CarOwner carOwner) {
        return new CarOwnerDTO(
                carOwner.getId(),
                carOwner.getName(),
                carOwner.car()
                        .map(Car::getBrand).orElse(""),
                carOwner.addresses()
                        .map(Address::getStreet)
                        .toArray(String[]::new),
                carOwner.getOccupations()
                        .stream()
                        .map(Occupation::getLabel)
                        .toArray(String[]::new)
        );
    }

    @Value
    public static class CarOwnerDTO {
        String id;
        String name;
        String brand;
        String[] addresses;
        String[] occupations;
    }

    @Value
    public static class CreateCarOwnerDTO {
        String name;

        @JsonCreator
        public CreateCarOwnerDTO(@JsonProperty("name") String name) {
            this.name = name;
        }
    }

    @Value
    public static class UpdateCarOwnerDTO {
        String name;

        @JsonCreator
        public UpdateCarOwnerDTO(@JsonProperty("name") String name) {
            this.name = name;
        }
    }

    @Value
    public static class AddCarDTO {
        String brand;

        @JsonCreator
        public AddCarDTO(@JsonProperty("brand") String brand) {
            this.brand = brand;
        }
    }

    @Value
    public static class AddAddressDTO {
        String street;

        @JsonCreator
        public AddAddressDTO(@JsonProperty("street") String street) {
            this.street = street;
        }
    }

    @Value
    public static class AddOccupationDTO {
        String label;

        @JsonCreator
        public AddOccupationDTO(@JsonProperty("label") String label) {
            this.label = label;
        }
    }
}
