package com.example.springbootdbcodealong.domain;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@Entity
@Table(name = "car_owner")
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarOwner {
    @Id
    String id;

    @Column(length = 100)
    String name;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "car_id", referencedColumnName = "id")
    Car car;

    @OneToMany(mappedBy = "carOwner", cascade = CascadeType.ALL)
    List<Address> addresses;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "car_owner_occupation",
            joinColumns = @JoinColumn(name = "car_owner_id"),
            inverseJoinColumns = @JoinColumn(name = "occupation_id"))
    List<Occupation> occupations;

    public CarOwner(String name) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.addresses = new ArrayList<>();
        this.occupations = new ArrayList<>();
    }

    public Optional<Car> car(){
        return Optional.ofNullable(car);
    }

    public void addAddress(Address address) {
        this.addresses.add(address);
    }

    public Stream<Address> addresses() {
        return addresses.stream();
    }

    public void addOccupation(Occupation occupation) {
        this.occupations.add(occupation);
    }

    public boolean hasOccupation(Occupation occupation) {
        return occupations.contains(occupation);
    }
}
