package com.example.springbootdbcodealong.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Address {
    @Id String id;

    @Column(length = 100)
    String street;

    @ManyToOne
    @JoinColumn(name = "car_owner_id")
    CarOwner carOwner;

    public Address(String street, CarOwner carOwner){
        this.id = UUID.randomUUID().toString();
        this.street = street;
        this.carOwner = carOwner;
    }
}
