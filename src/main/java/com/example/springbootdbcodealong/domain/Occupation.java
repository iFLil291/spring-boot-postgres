package com.example.springbootdbcodealong.domain;

import com.example.springbootdbcodealong.domain.CarOwner;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "occupation")
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Occupation {
    @Id
    String id;

    @Column(length = 100)
    String label;

    @ManyToMany(mappedBy = "occupations")
    List<CarOwner> carOwners;

    public Occupation(String label) {
        this.id = UUID.randomUUID().toString();
        this.label = label;
        this.carOwners = new ArrayList<>();
    }

    public void addCarOwner(CarOwner carOwner) {
        this.carOwners.add(carOwner);
    }
}
