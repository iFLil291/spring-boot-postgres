package com.example.springbootdbcodealong.domain;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "car")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Car {
    @Id String id;

    @Column(length = 100)
    String brand;

    @OneToOne(mappedBy = "car")
    CarOwner carOwner;

    public Car(String brand, CarOwner carOwner) {
        this.id = UUID.randomUUID().toString();
        this.brand = brand;
        this.carOwner = carOwner;
    }
}
