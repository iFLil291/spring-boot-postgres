package com.example.springbootdbcodealong;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDbCodealongApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDbCodealongApplication.class, args);
    }

}
