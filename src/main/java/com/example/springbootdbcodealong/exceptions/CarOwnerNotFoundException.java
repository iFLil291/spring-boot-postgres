package com.example.springbootdbcodealong.exceptions;

public class CarOwnerNotFoundException extends Exception {
    public CarOwnerNotFoundException(String msg){
        super(msg);
    }
}
