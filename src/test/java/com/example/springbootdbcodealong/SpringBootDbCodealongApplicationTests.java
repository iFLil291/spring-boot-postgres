package com.example.springbootdbcodealong;

import com.example.springbootdbcodealong.domain.CarOwner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
class SpringBootDbCodealongApplicationTests {

    @Autowired
    CarOwnerController carOwnerController;


    @Test
    void test_add_car_success(){
        // Given
        ResponseEntity<CarOwnerController.CarOwnerDTO> carOwnerDTO = carOwnerController.addCar("f6aea714-29a2-4d2d-a46c-a767c74e6b1e", new CarOwnerController.AddCarDTO("BMW"));
        String brand = "BMW";

        // Then
        assertEquals(brand, carOwnerDTO);
    }

}
